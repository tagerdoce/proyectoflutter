import 'dart:io';
import 'package:d_and_d_project/database/dbhelper.dart';
import 'package:d_and_d_project/characterlist.dart';
import 'package:d_and_d_project/model/character.dart';
import 'package:d_and_d_project/details/details_screen.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'SQFLite DataBase Demo',
      theme: new ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: new MyCharacterList(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

 Character employee = new Character(null,"", "", "", "","");
  
int id;
String ddname; 
String ddclass;
String ddrace;
String ddlevel;
File  galleryFile;

String image;
final scaffoldKey = new GlobalKey<ScaffoldState>();
final formKey = new GlobalKey<FormState>();

void imageSelectorGallery() async {
  galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);

  image = galleryFile.path;
}


    @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text('Creating Character'),
      ),
      body: new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Form(
          key: formKey,
          child: new Column(
            children: [
              new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(labelText: 'Hero Name'),
                validator: (val) =>
                    val.length == 0 ?"Enter character name" : null,
                onSaved: (val) => this.ddname = val,
              ),
              new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(labelText: 'Hero class'),
                validator: (val) =>
                    val.length ==0 ? 'Enter character class' : null,
                onSaved: (val) => this.ddclass = val,
              ),
              new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(labelText: 'Hero race'),
                validator: (val) =>
                    val.length ==0 ? 'Enter character race' : null,
                onSaved: (val) => this.ddrace = val,
              ),
              new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(labelText: 'Hero level'),
                validator: (val) =>
                    val.length ==0 ? 'Enter character level' : null,
                onSaved: (val) => this.ddlevel = val,
              ),
              new Container(margin: const EdgeInsets.only(top: 10.0),
              child: new RaisedButton(onPressed: _submit,child: new Text('Create'),),),

              new Container(margin: const EdgeInsets.only(top: 10.0),
              child: new RaisedButton(onPressed: imageSelectorGallery,child: new Text('Pick Image'),),)
            ],
          ),
        ),
      ),
    );
  }
  void _submit() {
     if (this.formKey.currentState.validate()) {
      formKey.currentState.save(); 
     }else{
       return null;
     }
    var character = Character(id,ddname,ddclass,ddrace,ddlevel,image);
    var dbHelper = DBHelper();
    dbHelper.saveEmployee(character);
    _showSnackBar("Character saved successfully");
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
  
    void openDetailScreen(int position) {
    Navigator.push(context, MaterialPageRoute(builder: (c) {
      return MyDetails(position);
    }));
  }
}