import 'dart:async';
import 'dart:io' as io;
import 'package:d_and_d_project/model/character.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "test.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE Character(id INTEGER PRIMARY KEY, ddname TEXT, ddclass TEXT, ddrace TEXT, ddlevel TEXT, image TEXT )");
    print("Created tables");
  }

  void saveEmployee(Character character) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawInsert(
          'INSERT INTO Character(ddname, ddclass, ddrace, ddlevel, image) VALUES(' +
              '\'' +
              character.ddname +
              '\'' +
              ',' +
              '\'' +
              character.ddclass +
              '\'' +
              ',' +
              '\'' +
              character.ddrace +
              '\'' +
              ',' +
              '\'' +
              character.ddlevel +
              '\'' +
              ',' +
              '\'' +
              character.image +
              '\'' +
              ')');
    });
  }

//Metodo de actualizacion que no funciona
    void updateEmployee(Character character) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawUpdate(
          'UPDATE Character SET ddname = '+ character.ddname + ', ddclass = '+character.ddclass + ', ddrace = '+ character.ddrace + ' WHERE id = '+ character.id.toString()+';'
          );
    });
  }

  Future<List<Character>> getCharacters() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM Character');
    List<Character> characters = new List();
    for (int i = 0; i < list.length; i++) {
      characters.add(new Character(list[i]["id"],list[i]["ddname"], list[i]["ddclass"], list[i]["ddrace"], list[i]["ddlevel"], list[i]["image"]));
    }
    return characters;
  }
}