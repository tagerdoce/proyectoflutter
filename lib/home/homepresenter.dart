import 'dart:convert';
import 'package:d_and_d_project/home/homeview.dart';
import 'package:http/http.dart' as network;

class HomePresenter {
  HomeView _view;

  HomePresenter(this._view);

  elementClicked(int position) {
    _view.openSettingsScreen(position);
  }
}