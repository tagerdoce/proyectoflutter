import 'package:d_and_d_project/database/dbhelper.dart';
import 'package:d_and_d_project/details/details_screen.dart';
import 'package:d_and_d_project/home/homepresenter.dart';
import 'package:d_and_d_project/home/homeview.dart';
import 'package:d_and_d_project/main.dart';
import 'package:d_and_d_project/model/character.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';

Future<List<Character>> fetchCharactersFromDatabase() async {
  var dbHelper = DBHelper();
  Future<List<Character>> characters = dbHelper.getCharacters();
  return characters;
}

class MyCharacterList extends StatefulWidget {
  @override
  MyCharacterListPageState createState() => new MyCharacterListPageState();
}

class MyCharacterListPageState extends State<MyCharacterList> implements HomeView{
  HomePresenter _homePresenter;

  MyCharacterListPageState() {
    _homePresenter = HomePresenter(this);
  }

  void _toCreate(){
    Navigator.push(context, MaterialPageRoute(builder: (c) {return MyHomePage();}));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Characters List'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(16.0),
        child: new FutureBuilder<List<Character>>(
          future: fetchCharactersFromDatabase(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return new ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                                          //Esto es la posicion en la lista, importante recordarlo

                    
                    return ListTile(
                    leading: new Image.file(File(snapshot.data[index].image),
                      fit:BoxFit.fill,
                      width: 100.0, 
                      height: 100.0,
                    ),

                    title:Text(snapshot.data[index].ddname,style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),

                    subtitle: Text(snapshot.data[index].ddclass,style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),

                    contentPadding: const EdgeInsets.fromLTRB(0.0, 14.0, 0.0, 25.0),
                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(builder: (c) {
                      return MyDetails(index);
                      }));
                    },
                    );
                  });
            } else if (snapshot.hasError) {
              return new Text("${snapshot.error}");
            }
            return new Container(alignment: AlignmentDirectional.center,child: new CircularProgressIndicator(),);
          },
        ),
      ),

      floatingActionButton: FloatingActionButton.extended(
        onPressed: _toCreate,
        icon: Icon(Icons.save),
        label: Text("New")
      ),
    );
  }

  @override
  openSettingsScreen(int position) {
    // TODO: implement openSettingsScreen
    return null;
  }
}