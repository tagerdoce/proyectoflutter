class Character{

  int id;
  String ddname;
  String ddclass;
  String ddrace;
  String ddlevel;
  String image;
  
  Character(this.id,this.ddname, this.ddclass,this.ddrace,this.ddlevel,this.image);
  
   Character.fromMap(Map map) {
    id = map[id];
    ddname = map[ddname];
    ddclass = map[ddclass];
    ddrace = map[ddlevel];
    ddlevel = map[ddlevel];
    image = map[image];
  }
  
}