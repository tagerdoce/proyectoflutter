import 'package:d_and_d_project/database/dbhelper.dart';
import 'package:d_and_d_project/model/character.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';

Future<List<Character>> fetchCharactersFromDatabase() async {
  var dbHelper = DBHelper();
  Future<List<Character>> characters = dbHelper.getCharacters();
  return characters;
}

class MyDetails extends StatefulWidget {
  final int position;

  MyDetails(this.position);

  @override
  MyDetailsPageState createState() => new MyDetailsPageState(position);
}

class MyDetailsPageState extends State<MyDetails> {

  int pos;

  int id;
  String ddname; 
  String ddclass;
  String ddrace;
  String ddlevel;
  String image;

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  MyDetailsPageState(this.pos);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Character Details'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(16.0),
        child: new FutureBuilder<List<Character>>(
          future: fetchCharactersFromDatabase(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {

              id = snapshot.data[pos].id;
              

              return Center(
                child: Column(
                  children: <Widget>[
                    new Image.file(File(snapshot.data[pos].image),
                      fit:BoxFit.fill,
                      width: 150.0, 
                      height: 150.0,
                    ),

                    new TextFormField(
                    decoration: new InputDecoration(labelText: 'Name'),
                    initialValue: snapshot.data[pos].ddname,
                    style: new TextStyle(fontSize:15.0,
                      color: const Color(0xFF000000),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Roboto"),
                      onSaved: (val) => this.ddname = val,
                  ),

                  new TextFormField(
                    decoration: new InputDecoration(labelText: 'Class'),
                    initialValue: snapshot.data[pos].ddclass,
                    style: new TextStyle(fontSize:15.0,
                      color: const Color(0xFF000000),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Roboto"),
                      onSaved: (val) => this.ddclass = val,
                  ),
    
                  new TextFormField(
                    decoration: new InputDecoration(labelText: 'Race'),
                    initialValue: snapshot.data[pos].ddrace,
                    style: new TextStyle(fontSize:15.0,
                      color: const Color(0xFF000000),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Roboto"),
                      onSaved: (val) => this.ddrace = val,
                  ),

                  new TextFormField(
                    decoration: new InputDecoration(labelText: 'Level'),
                    initialValue: snapshot.data[pos].ddlevel,
                    style: new TextStyle(fontSize:15.0,
                      color: const Color(0xFF000000),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Roboto"),
                      onSaved: (val) => this.ddlevel = val,
                  ),

                  //Boton del update sin funcionalidad
                  new Container(margin: const EdgeInsets.only(top: 10.0),
                  child: new RaisedButton(onPressed: _update,child: new Text('Update'),),),

                  ],
                ),
              );
              

            } else if (snapshot.hasError) {
              return new Text("${snapshot.error}");
            }
            return new Container(alignment: AlignmentDirectional.center,child: new CircularProgressIndicator(),);
          },
        ),
      ),
    );
  }
    void _update() {
    
     //Este metodo deberia de actualizar la base de datos en funcion de los datos cambiados, pero al final no a acabado funcionando

    //var employee = Employee(id,firstname,lastname,mobileno,emailId,image);
    //var dbHelper = DBHelper();
    //dbHelper.saveEmployee(employee);
  }

  
}